from kafka import KafkaConsumer
from json import loads
from time import sleep

consumer = KafkaConsumer(
    'mouse_observer',
    bootstrap_servers=['localhost:9092'],
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    group_id='my-group-id',
    value_deserializer=lambda x: loads(x.decode('utf-8'))
)

for msg in consumer:
    print(msg.value)
    sleep(0.5)