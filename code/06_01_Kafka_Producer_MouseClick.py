from json import dumps
from kafka import KafkaProducer
from pynput.mouse import Listener

producer = KafkaProducer(
    bootstrap_servers=['localhost:9092'],
    value_serializer=lambda x: dumps(x).encode('utf-8')
)


def on_move(x, y):
    print("Moved")
    data = {"event": f"moved to {x}, {y}"}
    producer.send('mouse_observer', value=data)  

def on_click(x, y, button, pressed):
    if pressed:
        print("Pressed")
        data = {"event": f"clicked at {x}, {y} with {button}"}
        producer.send('mouse_observer', value=data) 

def on_scroll(x, y, dx, dy):
    print("Scrolled")
    data = {"event": f"scrolled at {x}, {y} ({dx}, {dy})"}
    producer.send('mouse_observer', value=data) 


with Listener(on_move=on_move, on_click=on_click, on_scroll=on_scroll) as listener:
    listener.join()
