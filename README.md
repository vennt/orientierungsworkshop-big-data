# Orientierungsworkshop Big Data

Dieses Repository enthält die Unterlagen für den Orientierungsworkshop Big Data. 
Bitte beachten Sie, dass sie nur bis zum Ende der Woche Zugriff auf diese Dateien haben!

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/vennt%2Forientierungsworkshop-big-data/HEAD)
