## Kafka with Docker 

In your working directory, open a terminal and clone the GitHub repository of the docker 
image for Apache Kafka. Then change the current directory in the repository folder.


```
git clone https://github.com/wurstmeister/kafka-docker.git
cd kafka-docker
```

Inside kafka-docker, create a text file named docker-compose-expose.yml with 
the following content (you can use your favourite text editor):


```
version: '2'
services:
  zookeeper:
    image: wurstmeister/zookeeper:3.4.6
    ports:
     - "2181:2181"
  kafka:
    image: wurstmeister/kafka
    ports:
     - "9092:9092"
    expose:
     - "9093"
    environment:
      KAFKA_ADVERTISED_LISTENERS: INSIDE://kafka:9093,OUTSIDE://localhost:9092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: INSIDE:PLAINTEXT,OUTSIDE:PLAINTEXT
      KAFKA_LISTENERS: INSIDE://0.0.0.0:9093,OUTSIDE://0.0.0.0:9092
      KAFKA_INTER_BROKER_LISTENER_NAME: INSIDE
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_CREATE_TOPICS: "topic_test:1:1"
    volumes:
     - /var/run/docker.sock:/var/run/docker.sock
```

Now, you are ready to start the Kafka cluster with:

```
docker-compose -f docker-compose-expose.yml up
```

Don't forget to stop the container when finished.

```
docker-compose stop
```